<?php
class ControllerProductCatalog extends Controller {
    public function index($setting) {
        $this->load->language('product/catalog');

        $data['heading_title'] = $this->language->get('heading_title');
        $data['more_product'] = $this->language->get('more_product');
        $data['min_product'] = $this->language->get('min_product');


        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $data['search'] = $this->load->controller('common/search');

        if (isset($this->request->get['path'])) {
            $parts = explode('_', (string)$this->request->get['path']);
        } else {
            $parts = array();
        }

        if (isset($parts[0])) {
            $data['category_id'] = $parts[0];
        } else {
            $data['category_id'] = 0;
        }

        if (isset($parts[1])) {
            $data['child_id'] = $parts[1];
        } else {
            $data['child_id'] = 0;
        }

        $this->load->model('extension/extension');
        $this->load->model('tool/image');

        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {
            $children_data = array();

            $children = $this->model_catalog_category->getCategoriesHome($category['category_id']);

            foreach ($children as $child) {
                $filter_data = array(
                    'filter_category_id'  => $child['category_id'],
                    'filter_sub_category' => true
                );


                $children_data[] = array(
                    'category_id' => $child['category_id'],
                    'name'        => $child['name'],
                    'thumb'     => $this->model_tool_image->resize($child['image'], 290, 335),
//          'name'        => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                    'href'        => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),
                    'min_price'   => number_format(round($child['min_price'], (int)$this->currency->getDecimalPlace()), (int)$this->currency->getDecimalPlace(), '.', ' '),
                    'max_price'   =>number_format(round($child['max_price'], (int)$this->currency->getDecimalPlace()), (int)$this->currency->getDecimalPlace(), '.', ' '),
                );

            }

            $filter_data = array(
                'filter_category_id'  => $category['category_id'],
                'filter_sub_category' => true
            );

            $data['categories'][] = array(
                'category_id' => $category['category_id'],
                'name'        => $category['name'],
                'thumb'     => $this->model_tool_image->resize($category['image'], 290, 335),
//        'name'        => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                'children'    => $children_data,
                'href'        => $this->url->link('product/category', 'path=' . $category['category_id']),
            );
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/cataog.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/catalog.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/product/catalog.tpl', $data));
        }
    }
}