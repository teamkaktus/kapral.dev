	<div class="catalog-preview">
		<div class="catalog-preview__title">Наши двери</div>
		<div class="preview-list">
			<?php foreach ($categories as $category) { ?>
			<?php foreach ($category['children'] as $child) { ?>
			<div class="preview-item">
				<div class="preview-item__type"><span><?=$category['name']?>:&nbsp;</span><span class="mob">Вид:&nbsp;</span><?php echo $child['name'];?></div>
				<div class="preview-item__pict">
					<img src="<?= $child['thumb'];?>" alt="<?= $child['name'];?>">
				</div>
				<div class="preview-item__price">
					<p>Цены от <?=$child['min_price']; ?> до <?=$child['max_price']; ?> руб</p>
					<p class="mob">Цена: от <?=$child['min_price']; ?> руб.</p>
				</div>
				<a href="<?php echo $child['href'];?>" class="btn btn--preview"><span>Смотреть каталог</span></a>
			</div>
			<?php } ?>
			<?php } ?>
		</div>
		<a href="#" class="btn btn--list">Открыть полный список</a>
	</div>
