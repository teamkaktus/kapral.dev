<?php echo $header; ?>
    <div class="container content">
    <?php echo $column_left; ?>
    <div class="main">
        <?php echo $search; ?>

        <?php if ($products) { ?>
        <div class="catalog clearfix">
            <?php foreach ($products as $product) { ?>
            <div class="catalog-item">
                <div class="catalog-item__wrap">
                    <div class="catalog-item__type">Вид: <?php echo $heading_title; ?></div>
                    <div class="catalog-item__pict">
                        <span style="background-image: url('<?php echo $product['thumb']; ?>');  background-repeat: no-repeat"></span>
                    </div>
                    <a href="<?php echo $product['href']; ?>">
                        <div class="catalog-item__title"><?php echo $product['name']; ?></div>
                    </a>
                    <?php if ($product['price']) { ?>
                    <div class="catalog-item__price">
                        <?php if (!$product['special']) { ?>
                        <?php echo $product['price']; ?>
                        <?php } else { ?>
                        <span><?php echo $product['price']; ?></span>
                        <?php } ?>руб.
                    </div>
                    <?php } ?>
                    <?php if ($product['price']) { ?>
                    <div class="catalog-item__price catalog-item__price--mob">
                        <?php if (!$product['special']) { ?>
                        Цена: от <?php echo $product['price']; ?>
                        <?php } else { ?>
                        <span>Цена: от <?php echo $product['price']; ?></span>
                        <?php } ?>руб.
                    </div>
                    <?php } ?>
                    <div class="catalog-item__count product<?php echo $product['product_id']; ?>">
                        <div class="jq-number">
                            <div class="jq-number__field">
                                <input type="number" value="1" min="1" name="quantity">
                            </div>
                        </div>
                    </div>
                    <a data-pdqo-item="<?php echo $product['product_id']; ?>" onclick="pdqoObject(this);"
                       class="catalog-item__btn">Купить</a>
                    <a data-pdqo-item="<?php echo $product['product_id']; ?>" onclick="pdqoObject(this);"
                       class="btn btn--catalog"><span>Купить</span></a>
                    <a href="<?php echo $product['href']; ?>" class="catalog-info__btn">i</a>

                </div>
            </div>
            <?php } ?>

        </div>
        <div class="main">
            <div class="col-sm-6 text-left" style="margin-top: 42px; margin-bottom: 42px"><?php echo $pagination; ?></div>
            <div class="col-sm-6 text-right" style="text-align: right; margin-top: 42px; margin-bottom: 42px"><?php echo $results; ?></div>
        </div>
        <?php } else { ?>
        <p><?php echo $text_empty; ?></p>
        <?php } ?>
        <?php echo $content_bottom; ?>

    </div>
    </div>


<script type="text/javascript"><!--
    $('#button-search').bind('click', function () {
        url = 'index.php?route=product/search';

        var search = $('#content input[name=\'search\']').prop('value');

        if (search) {
            url += '&search=' + encodeURIComponent(search);
        }

        var category_id = $('#content select[name=\'category_id\']').prop('value');

        if (category_id > 0) {
            url += '&category_id=' + encodeURIComponent(category_id);
        }

        var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

        if (sub_category) {
            url += '&sub_category=true';
        }

        var filter_description = $('#content input[name=\'description\']:checked').prop('value');

        if (filter_description) {
            url += '&description=true';
        }

        location = url;
    });

    $('#content input[name=\'search\']').bind('keydown', function (e) {
        if (e.keyCode == 13) {
            $('#button-search').trigger('click');
        }
    });

    $('select[name=\'category_id\']').on('change', function () {
        if (this.value == '0') {
            $('input[name=\'sub_category\']').prop('disabled', true);
        } else {
            $('input[name=\'sub_category\']').prop('disabled', false);
        }
    });

    $('select[name=\'category_id\']').trigger('change');
    --></script>
<?php echo $footer; ?>