<?php echo $header; ?>
<div class="content clearfix">
    <div class="container">

        <?php echo $column_left; ?>

        <div class="main">
            <?php echo $search; ?>
            <div class="catalog-preview">
                <div class="catalog-preview__title">Наши двери</div>
                <div class="preview-list-catalog">
                    <?php foreach ($categories as $category) { ?>
                    <?php foreach ($category['children'] as $child) { ?>
                    <div class="preview-item">
                        <div class="preview-item__type"><span><?=$category['name']?>:&nbsp;</span><span class="mob">Вид:&nbsp;</span><?php echo $child['name'];?>
                        </div>
                        <div class="preview-item__pict">
                            <a href="<?php echo $child['href'];?>">
                                <img src="<?= $child['thumb'];?>" alt="<?= $child['name'];?>">
                            </a>
                        </div>
                        <div class="preview-item__price">
                            <p>Цены от <?=$child['min_price']; ?> до <?=$child['max_price']; ?> руб</p>
                            <p class="mob">Цена: от <?=$child['min_price']; ?> руб.</p>
                        </div>
                        <a href="<?php echo $child['href'];?>"
                           class="btn btn--preview"><span>Смотреть каталог</span></a>
                    </div>
                    <?php } ?>
                    <?php } ?>

                </div>

                <?php echo $content_top; ?>
                <?php echo $content_bottom; ?>
                <?php echo $column_right; ?>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>