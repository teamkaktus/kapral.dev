<?php echo $header; ?>
<div class="container content">
    <?php echo $column_left; ?>
      <div class="main">
      <div id="content" class="<?php echo $class; ?>">

        <?php echo $search; ?>
        <div class="container ffa1"><?php echo $column_right; ?></div>
          <div class="catalog clearfix">
            <?php echo $content_top; ?>
            <div class="catalog__title">Двери <?php echo $heading_title; ?></div>
            <?php if ($products) { ?>
            <div class="">
              <?php foreach ($products as $product) { ?>
              <div class="catalog-item">
                <div class="catalog-item__wrap">
                  <div class="catalog-item__type">Вид: <?php echo $heading_title; ?></div>
                  <div class="catalog-item__pict">
                    <a href="<?php echo $product['href']; ?>">
                    <span style="background-image: url('<?php echo $product['thumb']; ?>');  background-repeat: no-repeat"></span>
                    </a>
                  </div>
                  <a href="<?php echo $product['href']; ?>">
                    <div class="catalog-item__title" ><?php echo $product['name']; ?></div>
                  </a>
                  <?php if ($product['price']) { ?>
                  <div class="catalog-item__price">
                    <?php if (!$product['special']) { ?>
                    <?php echo $product['price']; ?>
                    <?php } else { ?>
                    <span><?php echo $product['price']; ?></span>
                    <?php } ?>руб.
                  </div>
                  <?php } ?>
                  <?php if ($product['price']) { ?>
                  <div class="catalog-item__price catalog-item__price--mob">
                    <?php if (!$product['special']) { ?>
                    Цена: от <?php echo $product['price']; ?>
                    <?php } else { ?>
                    <span>Цена: от <?php echo $product['price']; ?></span>
                    <?php } ?>руб.
                  </div>
                  <?php } ?>
                  <div class="catalog-item__count product<?php echo $product['product_id']; ?>">
                    <div class="jq-number">
                      <div class="jq-number__field">
                        <input type="number" value="1" min="1" name="quantity">
                      </div>
                    </div>
                  </div>
                  <a data-pdqo-item="<?php echo $product['product_id']; ?>"  onclick="pdqoObject(this);" class="catalog-item__btn cursor-pointer">Купить</a>
                  <a data-pdqo-item="<?php echo $product['product_id']; ?>"  onclick="pdqoObject(this);" class="btn btn--catalog cursor-pointer"><span>Купить</span></a>
                  <a href="<?php echo $product['href']; ?>" class="catalog-info__btn">i</a>
                  <div class="catalog-item__info">
                    <div class="description">
                      <?php $f=0 ?>
                      <?php if($product['attribute_groups']) { ?>
                      <?php foreach($product['attribute_groups'] as $attribute_group) { ?>
                      <?php if($f <= 3) { ?>
                      <?php foreach($attribute_group['attribute'] as $key => $attribute) { ?>
                      <p>
                                                             <span>
                                                                 <?php echo $attribute['name']; ?>
                                                             </span>
                        <?php echo $attribute['text']; ?>
                      </p>
                      <?php } ?>
                      <?php } ?>
                      <?php $f++ ?>
                      <?php } ?>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>
            <?php } ?>
            <?php if (!$categories && !$products) { ?>
            <p><?php echo $text_empty; ?></p>
            <div class="buttons">
              <div class="pull-right"><a href="<?php echo $continue; ?>"
                                         class="btn btn-primary"><?php echo $button_continue; ?></a></div>
            </div>
            <?php } ?>
            <?php echo $content_bottom; ?></div>
          <div class="pagination">
            <div class="pagination__title"><?php echo $pagination; ?></div>
            <div class="pagination-total"><?php echo $results; ?></div>
          </div>
        <?php if ($thumb || $description) { ?>
        <div class="">
          <!--<?php if ($thumb) { ?>
          <div class="col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
          <?php } ?>-->
          <?php if ($description) { ?>
          <div><?php echo $description2; ?></div>
          <?php } ?>
        </div>
        <?php } ?>
      </div>
      </div>
</div>
</div>
<?php echo $footer; ?>
