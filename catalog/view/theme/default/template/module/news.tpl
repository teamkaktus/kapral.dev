<div class="news-block">
	<div class="news-block__title">Последние новости</div>
	<?php foreach ($news as $news_item) { ?>
	<div class="news-item">
		<div class="news-item__date"><?php echo $news_item['posted']; ?></div>
		<div class="news-item__descr">
			<a href="<?php echo $news_item['href']; ?>" class="news-item__title"><?php echo $news_item['title']; ?></a>
			<div class="news-item__text">
				<p><?php echo $news_item['short_description']; ?></p>
			</div>
		</div>
	</div>
	<?php } ?>
	<a href="/index.php?route=information/news" class="news-item__link">Все новости</a>
</div>
