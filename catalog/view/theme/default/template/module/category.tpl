<div class="sidebar-catalog">
    <div class="sidebar-catalog__title">Каталог товаров<i></i></div>
    <div class="sidebar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['category_id'] == $category_id) { ?>
            <div class="sidebar-nav__title sidebar-nav__title--one"><?php echo $category['name']; ?><i></i>
            </div>
        <ul class="sidebar-menu">
            <?php if ($category['children']) { ?>
            <?php foreach ($category['children'] as $child) { ?>
            <?php if ($child['category_id'] == $child_id) { ?>
            <li class="active"><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?><i></i></a>
            </li>
            <?php } else { ?>
            <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?><i></i></a></li>
            <?php } ?>
            <?php } ?>
            <?php } ?>
        </ul>
        <?php } else { ?>
            <div class="sidebar-nav__title sidebar-nav__title--two"><?php echo $category['name']; ?><i></i>
            </div>
        <ul class="sidebar-menu">
            <?php if ($category['children']) { ?>
            <?php foreach ($category['children'] as $child) { ?>
            <?php if ($child['category_id'] == $child_id) { ?>
            <li class="active"><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?><i></i></a>
            </li>
            <?php } else { ?>
            <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?><i></i></a></li>
            <?php } ?>
            <?php } ?>
            <?php } ?>
        </ul>
        <?php } ?>
        <?php } ?>
    </div>
</div>
