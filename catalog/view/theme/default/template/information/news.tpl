<?php echo $header; ?>
<div class="content clearfix">
    <div class="container">
        <?php echo $column_left; ?>
        <div class="main">
            <?php echo $search; ?>
            <div class="news-item">
                <div class="news-item__descr">
                    <h2 class="news-item__title"><?php echo $heading_title; ?></h2>
                    </br>
                    <div class="news-item__date"><?php echo $posted; ?></div>
                    </br>
                    </br>
                    <div class="news-item__text">
                        <p><?php echo $description; ?></p>
                    </div>
                </div>
                    </br>
                <a style="     color: -webkit-link;   text-decoration: underline;" href="<?php echo $news_list; ?>"><?php echo $button_news; ?></a>
            </div>
        </div>
    </div>
</div>


<?php echo $footer; ?>