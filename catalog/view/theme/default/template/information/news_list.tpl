<?php echo $header; ?>
<div class="content clearfix">
	<div class="container">
		<?php echo $column_left; ?>
		<div class="main">
			<?php echo $content_top; ?>
			<?php echo $search; ?>
			<h1><?php echo $heading_title; ?></h1>
			<?php if ($news_list) { ?>

			<?php foreach ($news_list as $news_item) { ?>
			<div class="news-item">
				<div class="news-item__date"><?php echo $news_item['posted']; ?></div>
				<div class="news-item__descr">
					<a href=<?php echo $news_item['href']; ?>" class="news-item__title"><?php echo $news_item['title']; ?></a>
					<div class="news-item__text">
						<p><?php echo $news_item['short_description']; ?></p>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php } else { ?>
			<p><?php echo $text_empty; ?></p>
			<div class="buttons">
				<div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
			</div>
			<?php } ?>
			<?php echo $content_bottom; ?></div>
		<?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>