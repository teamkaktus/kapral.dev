<footer class="footer">
  <div class="container">
    <div class="contact">
      <div class="contact-social">
        <a href="<?php echo $facebook; ?>" class="contact-social__fb"></a>
        <a href="<?php echo $vkontakte; ?>" class="contact-social__vk"></a>
        <a href="<?php echo $twitter; ?>" class="contact-social__tw"></a>
        <a href="<?php echo $google; ?>" class="contact-social__gl"></a>
      </div>
      <div class="contact-phone">
        <div class="contact-phone__title">Свяжитесь с нами:</div>
        <a href="tel:<?php echo $telephone; ?>" class="contact-phone__link"><?php echo $telephone; ?></a>
        <a href="tel:<?php echo $telephone2; ?>" class="contact-phone__link"><?php echo $telephone2; ?></a>
      </div>
    </div>

    <div class="footer-nav footer-nav--catalog">
      <div class="footer-nav__title">Каталог товаров</div>
          <?php $first = '';?>
        <?php foreach($categories as $category){ ?>
          <?php if($category['sort_order'] == 0){ ?>
            <?php $first="footer-menu--first"; ?>
          <?php } ?>
          <ul class="footer-menu <?=$first; ?>">

          <?php foreach ($category['children'] as $child){ ?>
          <li><a href="<?=$child['href']?>"><?=$child['name']?></a></li>
          <?php }?>
          </ul>
        <?php $first = ''; ?>
        <?php }?>
    </div>

    <div class="footer-nav footer-nav--main">
      <div class="footer-nav__title">О магазине</div>
      <ul class="footer-menu">
        <li><a href="index.php?route=information/information&information_id=4">Как купить</a></li>
        <li><a href="index.php?route=information/information&information_id=6">Доставка</a></li>
        <li><a href="index.php?route=information/information&information_id=12">О магазине</a></li>
        <li><a href="index.php?route=information/information&information_id=7">Гарантия</a></li>
        <li><a href="index.php?route=information/contact">Контакты</a></li>
      </ul>
    </div>
    <div class="footer-accent">
      <p>Обращаем внимание на то, что данный интернет сайт носит исключительно информативный характер и ни в коем случае не является публичной офертой, согласно Статьи 437 ГК РФ. Все вопросы о стоимосте дверей и установки уточнйяте у менеджера по продаже. Производитель оставляет за собой право, менять комплектацию без предварительного уведомления</p>
    </div>

  </div>
</footer>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body>
</html>