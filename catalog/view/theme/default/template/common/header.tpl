<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=700, initial-scale=0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />
<script src="catalog/view/javascript/jquery/jquery-1.12.4.min.js" type="text/javascript"></script>
<!--<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />-->
<link href="catalog/view/javascript/bootstrap-grid/bootstrap-grid.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/formstyler/jquery.formstyler.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/magnific/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/modernizr/modernizr.js" type="text/javascript"></script>
<script src="catalog/view/javascript/magnific/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery-ui-1.12.1.custom/jquery-ui.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/slick/slick.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="catalog/view/javascript/formstyler/jquery.formstyler.css" rel="stylesheet" type="text/css" />

<link href="catalog/view/javascript/slick/slick.css" rel="stylesheet" type="text/css" />
<link href="catalog/view/javascript/formstyler/jquery.formstyler.css" rel="stylesheet" type="text/css" />
<link href="catalog/view/javascript/magnific/magnific-popup.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/fonts.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/main.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/sprite.css" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/pdqo/vendor/magnific-popup/magnific-popup.min.css" />
  <link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/pdqo/vendor/animate/animate.min.css" />
  <link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/pdqo/pdqo.min.css" />
  <link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/pdqo/default.css" />
  <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<script src="catalog/view/javascript/common2.js" type="text/javascript"></script>

<script src="catalog/view/javascript/fresco-2.0.3-light/js/fresco/fresco.js" type="text/javascript"></script>
<link rel="stylesheet" href="catalog/view/javascript/fresco-2.0.3-light/css/fresco/fresco.css"/>

<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body>

<header class="header clearfix">
  <div id="menu-mob" class="menu-mob">
    <ul class="menu-main">
      <li><a href="index.php?route=information/information&information_id=4">Как купить</a></li>
      <li><a href="index.php?route=information/information&information_id=6">Доставка</a></li>
      <li><a href="index.php?route=information/information&information_id=7">Гарантия</a></li>
      <li><a href="index.php?route=information/contact">Контакты</a></li>
      <li><a href="index.php?route=information/information&information_id=11">Акции</a></li>
    </ul>

    <ul class="catalog-menu">
      <?php foreach($categories as $category){ ?>
      <li class="catalog-menu__title"><?= $category['name']; ?></li>
          <?php foreach ($category['children'] as $child){ ?>
      <li><a href="<?= $child['href']; ?>"><?= $child['name']; ?><i></i></a></li>
          <?php } ?>
      <?php } ?>
    </ul>
  </div>
  <div class="container clearfix">
    <div class="logo-block">
        <a class="logo" href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
    </div>
    <div class="header-main">
      <div class="header-top">
        <div id="btn-mob" class="menu-toggle">
          <div class="menu-toggle__wrap">
            <b></b>
            <b></b>
            <b></b>
          </div>
          <div class="menu-toggle__title">Капрал</div>
        </div>
        <ul class="header-nav">
          <li><a href="index.php?route=information/information&information_id=4">Как купить</a></li>
          <li><a href="index.php?route=information/information&information_id=6">Доставка</a></li>
          <li><a href="index.php?route=information/information&information_id=7">Гарантия</a></li>
          <li><a href="index.php?route=information/contact">Контакты</a></li>
          <li class="accent"><a href="index.php?route=information/information&information_id=11">Акции</a></li>
        </ul>
        <div class="callback">
          <a class="callback-link callcons_b cursor-pointer">Обратный звонок</a>
          <span><?= $config_open; ?></span>
        </div>
      </div>
      <div class="header-phone">
        <a href="tel:<?php echo $telephone; ?>" class="header-phone__link"><?php echo $telephone; ?></a>
        <a href="tel:<?php echo $telephone2; ?>" class="header-phone__link"><?php echo $telephone2; ?></a>
      </div>
      <div class="header-call">
        <a class="header-call__request callcons_b cursor-pointer">Заказать звонок<i></i></a>
        <a class="header-call__measure callcons_b cursor-pointer">Вызов замерщика<i></i></a>
      </div>
      <div class="header-info">
        <span class="header-info__delivery">Бесплатная доставка от 10 000 р.</span>
        <span class="header-info__setup">Бесплатная установка от 17 000 р.</span>
      </div>
    </div>
  </div>
</header>

<!--<nav id="top">
  <div class="container">
    <?php echo $currency; ?>
    <?php echo $language; ?>
    <div id="top-links" class="nav pull-right">
      <ul class="list-inline">
        <li><a href="<?php echo $contact; ?>"><i class="fa fa-phone"></i></a> <span class="hidden-xs hidden-sm hidden-md"><?php echo $telephone; ?></span></li>
        <li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </li>
        <li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a></li>
        <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a></li>
        <li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_checkout; ?></span></a></li>
      </ul>
    </div>
  </div>
</nav>
<header>
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <div id="logo">
          <?php if ($logo) { ?>
            <?php if ($home == $og_url) { ?>
              <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?> class="img-responsive" />
            <?php } else { ?>
              <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
            <?php } ?>
          <?php } else { ?>
            <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>
      <div class="col-sm-5"><?php echo $search; ?>
      </div>
      <div class="col-sm-3"><?php echo $cart; ?></div>
    </div>
  </div>
</header>
<?php if ($categories) { ?>
<div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>
</div>
<?php } ?> -->
<script
        type="text/javascript"
        src="<?php echo HTTP_SERVER; ?>/callcons/js/callcons.js"
        charset='utf-8'>

</script>